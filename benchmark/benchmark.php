<?php

use Sungazer\Bundle\ClosureNormalizerBundle\Tests\BaseKernel;
use Sungazer\Bundle\ClosureNormalizerBundle\Tests\FullKernel;
use Sungazer\Bundle\ClosureNormalizerBundle\Tests\Utils\DataGenerator;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

require 'vendor/autoload.php';

$dataGen = new DataGenerator();


$baseKernel = new BaseKernel();
$baseKernel->boot();
$fullKernel = new FullKernel();
$fullKernel->boot();

/** @var Serializer $baseSerializer */
$baseSerializer = $baseKernel->getContainer()->get('serializer');
/** @var Serializer $fullSerializer */
$fullSerializer = $fullKernel->getContainer()->get('serializer');

$input  = new ArgvInput();
$output = new ConsoleOutput();

$io = new SymfonyStyle($input, $output);

$scenarios = [
    'single' => [
        'numBooks'        => 1,
        'numRelatedBooks' => 0,
        'dummyBookOpts'   => [
            'numAuthors' => 5,
        ]
    ],
    'simple' => [
        'numBooks'        => 50,
        'numRelatedBooks' => 0,
        'dummyBookOpts'   => [
            'numAuthors' => 5,
        ]
    ],
    'nested' => [
        'numBooks'        => 15,
        'numRelatedBooks' => 5,
        'dummyBookOpts'   => [
            'numAuthors' => 5,
        ]
    ],
];

$mdHandlerGen = function ($serializer) {
    return function ($attrValue, $object, $attribute, $format, $context) use ($serializer) {
        if ($object instanceof Book) {
            $context['groups'] = ['book:read:simple'];
            return $serializer->normalize($attrValue, $format, $context);
        }
        return null;
    };
};

$circHandlerGen = function ($serializer) {
    return function ($object, $format, $context) use ($serializer) {
        if ($object instanceof Book) {
            $context['groups'] = ['book:read:simple'];
            return $serializer->normalize($object, $format, $context);
        }
        return null;
    };
};

$serializerContext = [
    'groups'                                   => ['book:read', 'author:read', 'book:read:authors'],
    AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
];

$baseSerializerContext = array_merge($serializerContext, [
    AbstractObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => $circHandlerGen($baseSerializer),
    AbstractObjectNormalizer::MAX_DEPTH_HANDLER          => $mdHandlerGen($baseSerializer)
]);
$fullSerializerContext = array_merge($serializerContext, [
    AbstractObjectNormalizer::CIRCULAR_REFERENCE_HANDLER => $circHandlerGen($fullSerializer),
    AbstractObjectNormalizer::MAX_DEPTH_HANDLER          => $circHandlerGen($fullSerializer)
]);

$options = [
    'iterations' => 50,
    'warmup'     => true
];

foreach ($scenarios as $scenario) {
    $data = $dataGen->generateData($scenario);

    $table = [];

    $symfonyBaseRes = runBenchmark(function () use ($data, $baseSerializerContext, $baseSerializer) {
        return $baseSerializer->serialize($data, 'json', $baseSerializerContext);
    }, $options);
    $table[]        = ['Symfony standard', $symfonyBaseRes['time'], '', 1];

    $compiledRes = runBenchmark(function () use ($data, $fullSerializerContext, $fullSerializer) {
        return $fullSerializer->serialize($data, 'json', $fullSerializerContext);
    }, $options);
    $table[]     = ['Compiled', $compiledRes['time'], $compiledRes['result'] === $symfonyBaseRes['result'] ? 'Yes' : 'No', $symfonyBaseRes['time'] / $compiledRes['time']];

    $numIterations = $options['iterations'];
    $io->table(['Serializer', "Time (x$numIterations)", 'Equal?', 'Speedup Factor'], $table);
}


/*
 * Functions below
 */
function runBenchmark(callable $fn, array $options = [])
{
    $options = array_merge([
        'iterations' => 50,
        'warmup'     => true
    ], $options);

    if ($options['warmup'] === true) {
        // Warmup run for cache
        $res = $fn();
    }

    $start = microtime(true);
    for ($i = 0; $i < $options['iterations']; $i++) {
        $res = $fn();
    }
    $end = microtime(true);
    return [
        'time'   => $end - $start,
        'result' => $res
    ];
}