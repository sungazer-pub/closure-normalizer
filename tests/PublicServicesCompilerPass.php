<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class PublicServicesCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        $serviceIds = $container->getServiceIds();
        foreach($serviceIds as $serviceId){
            try {
                $def = $container->getDefinition($serviceId);
                $def->setPublic(true);
                $container->setDefinition($serviceId,$def);
            } catch (\Exception $e){

            }
        }
        $aliases = $container->getAliases();
        foreach($aliases as $alias){
            $alias->setPublic(true);
        }
    }
}