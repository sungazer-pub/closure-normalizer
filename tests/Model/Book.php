<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests\Model;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

class Book
{

    /**
     * @var string | null
     * @Groups({"book:read","book:read:simple"})
     */
    private $title;

    /**
     * @var string | null
     * @Groups({"book:read","book:read:simple"})
     */
    private $description;
    /**
     * @var Collection
     * @Groups({"book:read:authors"})
     */
    private $authors;
    /**
     * @var Collection
     * @Groups({"book:read"})
     * @MaxDepth(1)
     */
    private $relatedBooks;

    public function __construct()
    {
        $this->authors      = new ArrayCollection();
        $this->relatedBooks = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getRelatedBooks(): Collection
    {
        return $this->relatedBooks;
    }

    /**
     * @param Collection $relatedBooks
     * @return Book
     */
    public function setRelatedBooks(Collection $relatedBooks): Book
    {
        $this->relatedBooks = $relatedBooks;
        return $this;
    }

    /**
     * @return array
     * @Groups({"book:read:authors"})
     */
    public function getAuthorsArray(): array
    {
        return $this->authors->toArray();
    }

    /**
     * @Groups({"book:read:authors"})
     */
    public function getAuthorsArrayNonTyped()
    {
        return $this->authors->toArray();
    }

    /**
     * @Groups({"book:read"})
     */
    public function getNullProperty()
    {
        return null;
    }

    /**
     * @Groups({"book:read"})
     */
    public function getFloatProperty()
    {
        return 42.0;
    }

    /**
     * @param Author $author
     * @return Book
     */
    public function addAuthor(Author $author): Book
    {
        if (!$this->authors->contains($author)) {
            $this->authors->add($author);
        }
        return $this;
    }

    /**
     * @param Book $book
     * @return Book
     */
    public function addRelatedBook(Book $book): Book
    {
        if (!$this->relatedBooks->contains($book)) {
            $this->relatedBooks->add($book);
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Book
     */
    public function setTitle(?string $title): Book
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Book
     */
    public function setDescription(?string $description): Book
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    /**
     * @param Collection $authors
     * @return Book
     */
    public function setAuthors(Collection $authors): Book
    {
        $this->authors = $authors;
        return $this;
    }
}