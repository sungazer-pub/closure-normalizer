<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests\Model;


use Symfony\Component\Serializer\Annotation\Groups;

class Author
{

    /**
     * @var string | null
     * @Groups({"author:read"})
     */
    private $name;

    /**
     * @var string | null
     * @Groups({"author:read"})
     */
    private $surname;
    /**
     * @var string | null
     * @Groups({"author:read"})
     */
    private $email;

    public function __construct()
    {
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Author
     */
    public function setName(?string $name): Author
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return Author
     */
    public function setSurname(?string $surname): Author
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return Author
     */
    public function setEmail(?string $email): Author
    {
        $this->email = $email;
        return $this;
    }

}