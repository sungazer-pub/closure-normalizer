<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests;


use Sungazer\Bundle\ClosureNormalizerBundle\SungazerClosureNormalizerBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;

class FullKernel extends BaseKernel
{

    public function registerBundles()
    {
        $bundles   = parent::registerBundles();
        $bundles[] = new SungazerClosureNormalizerBundle();
        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        parent::registerContainerConfiguration($loader);
        $loader->load(function (ContainerBuilder $builder) {
            $builder->setParameter('kernel.secret', 'test');
            $builder->addCompilerPass(new PublicServicesCompilerPass());
        });
    }
}