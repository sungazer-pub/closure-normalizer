<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests;


use Sungazer\Bundle\ClosureNormalizerBundle\SungazerClosureNormalizerBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;

class BaseKernel extends Kernel
{

    public function __construct()
    {
        parent::__construct('test', true);
    }

    public function registerBundles()
    {
        return [
            new FrameworkBundle(),
        ];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $builder) {
            $builder->setParameter('kernel.secret', 'test');
            $builder->addCompilerPass(new PublicServicesCompilerPass());
        });
    }
}