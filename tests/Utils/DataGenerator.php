<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests\Utils;


use Faker\Factory;
use Sungazer\Bundle\ClosureNormalizerBundle\Tests\Model\Author;
use Sungazer\Bundle\ClosureNormalizerBundle\Tests\Model\Book;

class DataGenerator
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function generateData(array $options)
    {
        $options                    = array_merge([
            'numBooks'        => 150,
            'numRelatedBooks' => 1,
            'dummyBookOpts'   => [
                'numAuthors' => 10,
            ]
        ], $options);
        $options['numRelatedBooks'] = min($options['numRelatedBooks'], $options['numBooks']);

        $data = [];
        for ($i = 0; $i < $options['numBooks']; $i++) {
            $data[] = $this->createDummyBook($options['dummyBookOpts']);
        }
        // Populate "related books collection"
        foreach ($data as $book) {
            $eligibleBooks = array_filter($data, function($dataElem) use($book) {return $dataElem !== $book;});
            if (!$eligibleBooks) {
                continue;
            }
            $nb = min($options['numRelatedBooks'], count($eligibleBooks));
            if ($nb > 0) {
                foreach (ArrayUtils::randNElements($eligibleBooks, $nb) as $related) {
                    $book->addRelatedBook($related);
                }
            }
        }

        return $data;
    }

    private function createDummyBook(array $options = [])
    {
        $options = array_merge([
            'numAuthors' => 10,
        ], $options);
        $book    = new Book();
        $book->setTitle($this->faker->title)
            ->setDescription($this->faker->paragraph);
        for ($i = 0; $i < $options['numAuthors']; $i++) {
            $book->addAuthor($this->createDummyAuthor());
        }
        return $book;
    }

    private function createDummyAuthor()
    {
        $author = new Author();
        $author->setName($this->faker->firstName)
            ->setSurname($this->faker->lastName)
            ->setEmail($this->faker->email);
        return $author;
    }

}