<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests\Utils;


class ArrayUtils
{

    static function randElement(array $elements)
    {
        if (count($elements) === 0) {
            return null;
        }
        return $elements[array_rand($elements)];
    }

    static function randNElements(array $elements, int $n)
    {
        if ($n === 1) {
            yield $elements[array_rand($elements)];
        } else {
            foreach (array_rand($elements, $n) as $index) {
                yield $elements[$index];
            }
        }
    }

    static function arrayKeysExist(array $data, array $keys)
    {
        $ok = true;
        foreach ($keys as $key => $val) {
            if (is_array($val)) {
                if (array_key_exists($key, $data)) {
                    $ok = $ok && self::arrayKeysExist($data[$key], $val);
                } else {
                    $ok = false;
                }
            } else {
                $ok = $ok && array_key_exists($val, $data);
            }
        }
        return $ok;
    }

    static function arrayToMap($data, $fn)
    {
        $map = [];
        foreach ($data as $elem) {
            $map[$fn($elem)] = $elem;
        }
        return $map;
    }

    static function getFirstMatchingKey(array $data, array $keys)
    {
        foreach ($keys as $key) {
            if (array_key_exists($key, $data)) {
                return $key;
            }
        }
        return null;
    }

    static function invokeBatched(array $data, int $batchSize, $fn){
        $batches = (int)ceil(count($data) / (float)$batchSize);
        $result = [];
        for($i=0;$i<$batches;$i++){
            $subArray = array_slice($data,$i*$batchSize,$batchSize);
            $result[] = $fn($subArray);
        }
        return $result;
    }
}