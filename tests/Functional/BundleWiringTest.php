<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Tests\Functional;


use PHPUnit\Framework\TestCase;
use Sungazer\Bundle\ClosureNormalizerBundle\Serializer\Normalizer\ClosureNormalizer;
use Sungazer\Bundle\ClosureNormalizerBundle\Tests\BaseKernel;
use Sungazer\Bundle\ClosureNormalizerBundle\Tests\FullKernel;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class BundleWiringTest extends TestCase
{

    public function testWiring(){
        $kernel = new FullKernel();
        $kernel->boot();
        $container = $kernel->getContainer();

        $service = $container->get(ClosureNormalizer::class);
        $this->assertInstanceOf(ClosureNormalizer::class, $service);

        /** @var Serializer $serializer */
        $serializer = $container->get(SerializerInterface::class);

        $rClass = new \ReflectionClass($serializer);
        $rNormalizers = $rClass->getProperty('normalizers');
        $rNormalizers->setAccessible(true);
        $normalizers = $rNormalizers->getValue($serializer);
        $this->assertContains($service,$normalizers);
    }


}