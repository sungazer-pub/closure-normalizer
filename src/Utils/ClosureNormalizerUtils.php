<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Utils;


use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\Closure;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Expression;
use PhpParser\PrettyPrinter\Standard;

/**
 * @internal
 *
 * @author Luca Nardelli <luca@sungazer.io>
 */
class ClosureNormalizerUtils
{
    public static function createGetterCall(string $arrayVarName, string $arrayKey, string $objectName, string $getter)
    {
        return new Expression(
            new Assign(
                new ArrayDimFetch(new Variable($arrayVarName), new String_($arrayKey)),
                new MethodCall(new Variable($objectName), new Identifier($getter))
            )
        );
    }

    public static function getClosureFromClosureAST(Closure $closure): callable
    {
        $prettyPrinter = new Standard();
        $funcString    = $prettyPrinter->prettyPrint([new Expression($closure)]);
        $func          = eval("return " . $funcString);
        return $func;
    }
}
