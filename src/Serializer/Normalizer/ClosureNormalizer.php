<?php


namespace Sungazer\Bundle\ClosureNormalizerBundle\Serializer\Normalizer;


use ArrayObject;
use Exception;
use PhpParser\BuilderFactory;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\Closure;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\Return_;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use ReflectionNamedType;
use ReflectionObject;
use RuntimeException;
use Sungazer\Bundle\ClosureNormalizerBundle\Utils\ClosureNormalizerUtils;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Traversable;
use function count;
use function get_class;
use function is_array;
use function is_callable;
use function is_object;
use function strlen;

/**
 * Normalizes objects with getter and setter methods to arrays.
 *
 * For each object, the normalizer builds a closure that calls all of its public "getter"
 * methods (methods which have a name starting with get and take no parameters).
 * The result is a map from property names (method name stripped of the get prefix
 * and converted to lower case) to property values.
 * Property values are normalized through the serializer.
 *
 * Denormalization is not yet supported
 *
 * @author Luca Nardelli <luca@sungazer.io>
 */
class ClosureNormalizer extends AbstractObjectNormalizer implements CacheableSupportsMethodInterface
{
    public const ENABLE_CIRCULAR_REFERENCE = 'circular_reference_limit';
    private $closureCache = [];

    public function __construct(ClassMetadataFactoryInterface $classMetadataFactory = null,
                                NameConverterInterface $nameConverter = null,
                                PropertyTypeExtractorInterface $propertyTypeExtractor = null,
                                ClassDiscriminatorResolverInterface $classDiscriminatorResolver = null,
                                callable $objectClassResolver = null,
                                array $defaultContext = [])
    {
        parent::__construct($classMetadataFactory,
            $nameConverter,
            $propertyTypeExtractor,
            $classDiscriminatorResolver,
            $objectClassResolver,
            $defaultContext);
    }

    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        throw new Exception('not implemented');
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return false;
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        if (!isset($context['cache_key'])) {
            $context['cache_key'] = $this->getCacheKey($format, $context);
        }

        if (($context[self::ENABLE_CIRCULAR_REFERENCE] ?? true) && $this->isCircularReference($object, $context)) {
            // Need to invalidate cache key when we call the handler, in case the normalizer is called again with different groups
            unset($context['cache_key']);
            return $this->handleCircularReference($object, $format, $context);
        }

        $class = get_class($object);

        /*
         * NOTE: From here onwards, the keys of data are the NORMALIZED attribute names, along with all other keys in the metadata arrays.
         * We need to denormalize them when calling max_depth_handler or when descending into child contexts.
         * The metadata arrays contains the explicit direct and inverse mappings we need
         */
        $closureMeta = $this->getClosure($object, $format, $context);
        $data        = $closureMeta['func']($object);

        /*
         * Process attributes that have a max_depth property
         */
        if (($mdAttributes = $closureMeta['maxDepth'])
            && ($context[self::ENABLE_MAX_DEPTH] ?? $this->defaultContext[self::ENABLE_MAX_DEPTH] ?? false)) {

            if (isset($context[self::MAX_DEPTH_HANDLER])) {
                $maxDepthHandler = $context[self::MAX_DEPTH_HANDLER];
                if (!is_callable($maxDepthHandler)) {
                    throw new InvalidArgumentException(sprintf('The "%s" given in the context is not callable.', self::MAX_DEPTH_HANDLER));
                }
            } else {
                $maxDepthHandler = null;
            }

            foreach ($mdAttributes as $attribute => $maxDepth) {
                if ($maxDepthReached = $this->isMaxDepthReached($maxDepth, $class, $attribute, $context)) {
                    if (!$maxDepthHandler) {
                        unset($data[$attribute]);
                    } else {
                        // Need to invalidate cache key when we call the handler, in case the normalizer is called again with different groups
                        unset($context['cache_key']);
                        $data[$attribute] = $maxDepthHandler($data[$attribute], $object, $closureMeta['nameConversion']['inverse'][$attribute], $format, $context);
                    }
                }
            }
        }

        /*
         * If any callbacks have been defined, process them by applying them to the $data array
         */
        if (isset($context[self::CALLBACKS])) {
            if (!is_array($context[self::CALLBACKS])) {
                throw new InvalidArgumentException(sprintf('The "%s" context option must be an array of callables.', self::CALLBACKS));
            }

            foreach ($context[self::CALLBACKS] as $attribute => $callback) {
                if (!is_callable($callback)) {
                    throw new InvalidArgumentException(sprintf('Invalid callback found for attribute "%s" in the "%s" context option.', $attribute, self::CALLBACKS));
                }
            }
            foreach ($closureMeta['nameConversion']['inverse'] as $normalizedAttribute => $attribute) {
                /**
                 * @var callable|null
                 */
                $callback = $context[self::CALLBACKS][$attribute] ?? $this->defaultContext[self::CALLBACKS][$attribute] ?? null;
                if ($callback) {
                    $data[$normalizedAttribute] = $callback($data[$normalizedAttribute], $object, $attribute, $format, $context);
                }
            }
        }


        /*
         * Lastly, process children objects that have not been yet normalized
         */
        foreach ($closureMeta['stack'] as $normalizedAttribute => $val) {
            if (!$this->serializer instanceof NormalizerInterface) {
                throw new LogicException(sprintf('Cannot normalize attribute "%s" because the injected serializer is not a normalizer.', $normalizedAttribute));
            }
            if (!isset($data[$normalizedAttribute])) {
                continue;
            }
            $data[$normalizedAttribute] = $this->serializer->normalize(
                $data[$normalizedAttribute],
                $format,
                $this->createChildContext($context, $closureMeta['nameConversion']['inverse'][$normalizedAttribute], $format)
            );
        }

        if (isset($context[self::PRESERVE_EMPTY_OBJECTS]) && !count($data)) {
            return new ArrayObject();
        }

        return $data;
    }

    /**
     * Builds the cache key for the attributes cache.
     *
     * The key must be different for every option in the context that could change which attributes should be handled.
     *
     * @return bool|string
     */
    private function getCacheKey(?string $format, array $context)
    {
        foreach ($context[self::EXCLUDE_FROM_CACHE_KEY] ?? $this->defaultContext[self::EXCLUDE_FROM_CACHE_KEY] as $key) {
            unset($context[$key]);
        }
        unset($context[self::EXCLUDE_FROM_CACHE_KEY]);
        unset($context[self::OBJECT_TO_POPULATE]);
        unset($context['cache_key']); // avoid artificially different keys
        // These can be unset as their presence does not affect which attributes should be normalized
        unset($context[self::CIRCULAR_REFERENCE_HANDLER]);
        unset($context[self::MAX_DEPTH_HANDLER]);
        unset($context[self::CALLBACKS]);

        try {
            return md5($format . serialize([
                    'context' => $context,
                    'ignored' => $context[self::IGNORED_ATTRIBUTES] ?? $this->defaultContext[self::IGNORED_ATTRIBUTES],
                ]));
        } catch (Exception $exception) {
            // The context cannot be serialized, skip the cache
            return false;
        }
    }

    /**
     * @param $object
     * @param string|null $format
     * @param array $context
     * @return array|mixed
     */
    private function getClosure($object, string $format = null, array $context = [])
    {
        $class = get_class($object);
        $key   = $class . "-" . $context['cache_key'];

        if ($context['cache_key'] && isset($this->closureCache[$key])) {
            return $this->closureCache[$key];
        }

        $attributes = $this->getAttributes($object, $format, $context);

        $attributesMetadata = $this->classMetadataFactory ? $this->classMetadataFactory->getMetadataFor($class)->getAttributesMetadata() : null;
        $accessorMetadata   = $this->getAccessorMetadata($class);

        $factory = new BuilderFactory();
        $closure = new Closure([
            'params' => [
                $factory->param('o')->getNode()
            ],
            'stmts'  => [
                new Expression(new Assign(new Variable('data'), new Array_())),
            ],
        ]);

        $stack          = []; // Used to normalize children
        $maxDepth       = []; // Used to track serialization depth for the properties where maxdepth is enabled
        $nameConversion = [   // Used to track name conversion
            'direct'  => [],
            'inverse' => []
        ];

        foreach ($attributes as $attribute) {
            // Skip attributes for which we do not have an accessor method
            if(!array_key_exists($attribute,$accessorMetadata)){
                continue;
            }
            // Generate direct and inverse name mappings
            if ($this->nameConverter) {
                $normalizedAttribute = $this->nameConverter->normalize($attribute, $class, $format, $context);
            } else {
                $normalizedAttribute = $attribute;
            }
            $nameConversion['direct'][$attribute]            = $normalizedAttribute;
            $nameConversion['inverse'][$normalizedAttribute] = $attribute;

            // If an attribute has a max_depth metadata property, add it to our tracking list
            if ($attributesMetadata && ($md = $attributesMetadata[$attribute]->getMaxDepth())) {
                $maxDepth[$normalizedAttribute] = $md;
            }

            // If we have return type information, we can know if we will have to call the serializer again, this avoids some checks later on
            /** @var ReflectionNamedType $rt */
            if ($rt = $accessorMetadata[$attribute]['returnType'] ?? null) {
                $rtName = $rt->getName();
                if (!$rt->isBuiltin() || in_array($rtName, ['array', 'object', 'resource'])) {
                    $stack[$normalizedAttribute] = true;
                }
            } else {
                $stack[$normalizedAttribute] = true;
            }

            // Add getter to the closure we are building
            $closure->stmts[] = ClosureNormalizerUtils::createGetterCall('data', $normalizedAttribute, 'o', $accessorMetadata[$attribute]['methodName']);
        }
        $closure->stmts[] = new Return_(new Variable('data'));

        $func = ClosureNormalizerUtils::getClosureFromClosureAST($closure);

        $result = [
            'func'           => $func,
            'stack'          => $stack,
            'maxDepth'       => $maxDepth,
            'nameConversion' => $nameConversion,
        ];

        if ($context['cache_key']) {
            $this->closureCache[$key] = $result;
            return $this->closureCache[$key];
        } else {
            return $result;
        }
    }

    /**
     * Is the max depth reached for the given attribute?
     *
     * @param int $maxDepth
     * @param string $class
     * @param string $attribute
     * @param array $context
     * @return bool
     */
    private function isMaxDepthReached(int $maxDepth, string $class, string $attribute, array &$context): bool
    {
        $key = sprintf(self::DEPTH_KEY_PATTERN, $class, $attribute);
        if (!isset($context[$key])) {
            $context[$key] = 1;

            return false;
        }

        if ($context[$key] === $maxDepth) {
            return true;
        }

        ++$context[$key];

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function createChildContext(array $parentContext, string $attribute, ?string $format): array
    {
        // Do not call AbstractObjectNormalizer createChildContext because it calls the wrong getCacheKey function as it is private
        $context              = AbstractNormalizer::createChildContext($parentContext, $attribute, $format);
        $context['cache_key'] = $this->getCacheKey($format, $context);
        return $context;
    }

    /**
     * Returns metadata associated to an accessor (getter method).
     * For now, return type is supported
     * @param string $class
     * @return array = [
     *     'attributeName' => [
     *       'methodName' => 'getAttribute',
     *       'returnType' => new ReflectionType(),
     *     ]
     * ]
     * @throws ReflectionException
     */
    private function getAccessorMetadata(string $class)
    {
        $class   = new ReflectionClass($class);
        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $meta    = [];
        foreach ($methods as $method) {
            if (!$this->isGetMethod($method)) {
                continue;
            }
            $attributeName        = lcfirst(substr($method->name, 0 === strpos($method->name, 'is') ? 2 : 3));
            $meta[$attributeName] = [
                'methodName' => $method->name,
                'returnType' => $method->getReturnType()
            ];
        }
        return $meta;
    }

    /**
     * Checks if a method's name is get.*|is.*|has.*, and can be called without parameters.
     */
    private function isGetMethod(ReflectionMethod $method): bool
    {
        $methodLength = strlen($method->name);

        return
            !$method->isStatic() &&
            (
                ((0 === strpos($method->name, 'get') && 3 < $methodLength) ||
                    (0 === strpos($method->name, 'is') && 2 < $methodLength) ||
                    (0 === strpos($method->name, 'has') && 3 < $methodLength)) &&
                0 === $method->getNumberOfRequiredParameters()
            );
    }

    public function supportsNormalization($data, string $format = null)
    {
        return (is_object($data) && !$data instanceof Traversable)
            && $this->supports(get_class($data));
    }

    /**
     * Checks if the given class has any get{Property} method.
     */
    private function supports(string $class): bool
    {
        $class   = new ReflectionClass($class);
        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        foreach ($methods as $method) {
            if ($this->isGetMethod($method)) {
                return true;
            }
        }

        return false;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }

    protected function getAttributeValue(object $object, string $attribute, string $format = null, array $context = [])
    {
        throw new RuntimeException('this should not be called');
    }

    protected function extractAttributes(object $object, string $format = null, array $context = [])
    {
        $reflectionObject  = new ReflectionObject($object);
        $reflectionMethods = $reflectionObject->getMethods(ReflectionMethod::IS_PUBLIC);

        $attributes = [];
        foreach ($reflectionMethods as $method) {
            if (!$this->isGetMethod($method)) {
                continue;
            }

            $attributeName = lcfirst(substr($method->name, 0 === strpos($method->name, 'is') ? 2 : 3));

            if ($this->isAllowedAttribute($object, $attributeName, $format, $context)) {
                $attributes[] = $attributeName;
            }
        }

        return $attributes;
    }

    protected function setAttributeValue(object $object, string $attribute, $value, string $format = null, array $context = [])
    {
        throw new RuntimeException('this should not be called');
    }
}
