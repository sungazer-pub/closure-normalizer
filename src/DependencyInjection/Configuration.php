<?php

namespace Sungazer\Bundle\ClosureNormalizerBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sungazer_closure_normalizer');

        $rootNode = $treeBuilder->getRootNode();

        $rootNode->children()
            ->scalarNode('enabled')
            ->defaultValue(true)->end();

        return $treeBuilder;
    }
}
