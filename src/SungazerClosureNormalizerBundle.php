<?php

namespace Sungazer\Bundle\ClosureNormalizerBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * This bundle implements a new normalizer for Symfony
 * that uses closures generated at runtime to speed up the normalization process
 *
 * Author: Luca Nardelli <luca@sungazer.io>
 */

class SungazerClosureNormalizerBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}

